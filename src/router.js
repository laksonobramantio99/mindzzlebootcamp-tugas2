import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'profile',
      component: () =>
        import('./views/ProfilePage.vue'),
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () =>
        import('./views/DashboardPage.vue'),
    },
    {
      path: '/employee',
      name: 'employee',
      component: () =>
        import('./views/EmployeePage.vue'),
    },
    {
      path: '/jobposting',
      name: 'jobposting',
      component: () =>
        import('./views/JobPostingPage.vue'),
    },
    {
      path: '/hierarchy',
      name: 'hierarchy',
      component: () =>
        import('./views/HierarchyPage.vue'),
    },
  ],
});
